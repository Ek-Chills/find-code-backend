import express, { Request, Response } from "express";
import { pc, openai } from "../lib/utils";
import {v4 as uuid} from 'uuid'


 const router = express.Router()

router.post('/analyzeCode', async(req:Request, res:Response) => {
  const fileContents: { name: string; content: string }[] = req.body
  const indexName = "find-code-index";

  try {
    for (const fileContent of fileContents) {
      const embedding = await openai.embeddings.create({
        model: "text-embedding-3-small",
        input: fileContent.content,
        encoding_format: "float",
      });

      const index = pc.index(indexName);

      await index.namespace("ns1").upsert([
        {
          id: uuid(),
          values: embedding.data[0].embedding,
          metadata: {
            codeContent: fileContent.content,
            fileName: fileContent.name,
          },
        },
      ]);
    }

    return res.json({
      success: true,
    });
  } catch (error) {
    res.send(error)
  }
})

router.get('/searchCode', async(req:Request, res:Response) => {
  const query = req.query.query
  console.log(query);
  
  try {
    const queryEmbedding = await openai.embeddings.create({
      model: "text-embedding-3-small",
      input: query as string,
      encoding_format: "float",
    });
    const index = pc.index("find-code-index")
    const queryResponse = await index.namespace("ns1").query({
      topK: 2,
      vector: queryEmbedding.data[0].embedding,
      includeValues: true,
      includeMetadata:true
    });
    console.log(queryResponse);
    return res.json({matches:queryResponse.matches, success:true})
  } catch (error) {
    console.log(error);
    
    res.send(error)
  }
})


export default router