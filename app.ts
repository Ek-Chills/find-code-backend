import express, {Express} from 'express'
import cors from 'cors'
import router from './routers/embeddings'

const app: Express = express()
const port = 3000

app.use(cors())
app.use(express.json())

app.use('/api/v1', router)


app.listen(port, () => {
  console.log(`server is running on port ${port}`);
  
})